#ifndef TEXT_ROLL_H
# define TEXT_ROLL_H

#include <types.h>
#include <ft_list.h>

//#define DESIGNATE_NEW(T, ...) (T*)memcpy(malloc(sizeof(T)),&(T const){__VA_ARGS__},sizeof(T))
#define CREATE(T, name)	T	*name = (T*)malloc(sizeof(T))
#define DESIGNATE_NEW(T, ...)	(T*)memcpy(malloc(sizeof(T)), &(const T) {__VA_ARGS__}, sizeof(T))


#define FAST_SPEED_DELAY	10
#define MEDIUM_SPEED_DELAY	25
#define NORMAL_SPEED_DELAY	50

typedef uint8_t	t_font_id;


typedef struct	s_rolled_text
{	
	uint8_t		id;
	uint8_t		startX;
	uint8_t		startY;
	uint8_t		endX;
	uint8_t		speed;
	uint8_t		fontID;
	char		*str;
}				t_rolled_text;

typedef enum	e_direction
{
	direction_right = 0,
	direction_left = 1
}				t_direction;

typedef struct	s_rolled_buff
{
	uint8_t		text_id;
	uint16_t	buff_len;
	uint16_t	show_len;
	uint16_t	startX;
	t_direction	roll_direction;
	uint8_t		*buff;
}				t_rolled_buff;

// I am sorry :(((
// There is no other way arround :(((
extern t_list			*head;

void			add_rolled_text(
					const uint8_t x, const uint8_t y,
					const uint16_t show_len, t_direction direction,
					const char *str, t_font_id font_id); 
void			roll_text(void);

#endif