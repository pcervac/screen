#ifndef ROLL_TEXT_HIDDEN_H
# define ROLL_TEXT_HIDDEN_H

#include <text_roll.h>
#include <ft_list.h>

#define ROLLED_TEXTS_ARRAY_LENGTH	10
extern t_rolled_text *rolled_texts_array[ROLLED_TEXTS_ARRAY_LENGTH];

#endif //ROLL_TEXT_HIDDEN_H
