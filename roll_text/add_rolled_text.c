#include <text_roll.h>
#include <text_roll_hidden.h>
#include <stddef.h>
#include <font.h>
#include <disp1color.h>
// I am sorry :(((
// There is no other way arround :(((
//t_rolled_buff	buffs[] =
//{ 
//	{
//		.show_len = 30,
//		.buff_len = 30 * 2,
//		.startX = 150,
//		.roll_direction = direction_right,
//		.buff = {
//			0x7f, 0x08, 0x08, 0x7f, 0x00, 0x00,
//			0x7f, 0x49, 0x49, 0x49, 0x00, 0x00,
//			0x7f, 0x40, 0x40, 0x40, 0x00, 0x00,
//			0x7f, 0x40, 0x40, 0x40, 0x00, 0x00,
//			0x3e, 0x41, 0x41, 0x3e, 0x00, 0x00
//		}
//	},
//	{
//		.show_len = 30,
//		.buff_len = 30 * 2,
//		.startX = 0,
//		.roll_direction = direction_left,
//		.buff = {
//			0x7f, 0x08, 0x08, 0x7f, 0x00, 0x00,
//			0x7f, 0x49, 0x49, 0x49, 0x00, 0x00,
//			0x7f, 0x40, 0x40, 0x40, 0x00, 0x00,
//			0x7f, 0x40, 0x40, 0x40, 0x00, 0x00,
//			0x3e, 0x41, 0x41, 0x3e, 0x00, 0x00
//		}
//	}
//};

static uint16_t	get_strdlen(const char *str, t_font_id font_id)
{
	uint16_t	length = 0;

	while (*str != '\0')
	{
		uint8_t	*char_table	= font_GetFontStruct(font_id, *str);
		uint8_t	char_widht = font_GetCharWidth(char_table);
		length += char_widht;
		str++;
	}
	return length;
}

static uint8_t	store_char_in_buff_small(
	uint8_t *buff,
	const uint16_t start_x, const t_font_id font_id, const uint8_t stored_char)
{
	uint8_t	*char_table	= font_GetFontStruct(font_id, stored_char);
	uint8_t	char_widhth = font_GetCharWidth(char_table);
	uint8_t char_height = font_GetCharHeight(char_table);
	
	char_table += 2;
	for (uint8_t row = 0; row != char_height; row++)
	{
		uint8_t	char_line = *char_table++;
		for (uint8_t col = 0; col != char_widhth; col++)
			buff[start_x + col] |= (((char_line >> (7 - col)) & 1) << row);
	}
	return (char_widhth);
}

static uint8_t	store_char_in_buff_big(
	uint8_t *buff1, uint8_t *buff2,
	const uint16_t start_x, const t_font_id font_id, const uint8_t stored_char)
{
	uint8_t	*char_table	= font_GetFontStruct(font_id, stored_char);
	uint8_t	char_widhth = font_GetCharWidth(char_table);
	uint8_t char_height = font_GetCharHeight(char_table);
	
	char_table += 2;
	for (uint8_t row = 0; row != char_height; row++)
	{
		uint8_t *buff = row < 8 ? buff1 : buff2;
		uint8_t	char_line1 = *char_table++;
		uint8_t	char_line2 = *char_table++;
		for (uint8_t col = 0; col != char_widhth; col++)
		{
			uint8_t char_line = col < 8 ? char_line1 : char_line2;
			buff[start_x + col] |= (((char_line >> (7 - (col % 8))) & 1) << (row % 8));
		}
	}
	return (char_widhth);
}

void			add_rolled_text(
					const uint8_t x, const uint8_t y,
					const uint16_t show_len, t_direction direction,
					const char *str, t_font_id font_id)
{
	uint16_t		str_dlen = get_strdlen(str, font_id);
	uint8_t			buff_index = 0;
	t_rolled_buff	rolled_buff = {
		.buff_len = str_dlen + show_len,
		.roll_direction = direction,
		.show_len = show_len,
		.startX = DISP1COLOR_Width * y + x,
		.roll_direction = direction_left
	};

	if (font_id == FONTID_6X8M)
	{
		uint8_t		*buff =
			(uint8_t*)memset(malloc(str_dlen * 2), 0, str_dlen * 2);	
		while (*str != '\0')
			buff_index +=
				store_char_in_buff_small(buff, buff_index, font_id, *str++);
		rolled_buff.buff = buff;
		ft_lstadd(&head, ft_lstnew(&rolled_buff, sizeof(rolled_buff)));
	}
	else
	{
		uint8_t		*buff1 =
			(uint8_t*)memset(malloc(str_dlen * 2), 0, str_dlen * 2);	
		uint8_t		*buff2 =
			(uint8_t*)memset(malloc(str_dlen * 2), 0, str_dlen * 2);	
		while (*str != '\0')
			buff_index +=
				store_char_in_buff_big(buff1, buff2, buff_index, font_id, *str++);
		rolled_buff.buff = buff1;
		ft_lstadd(&head, ft_lstnew(&rolled_buff, sizeof(rolled_buff)));
		rolled_buff.buff = buff2;
		rolled_buff.startX += DISP1COLOR_Width;
		ft_lstadd(&head, ft_lstnew(&rolled_buff, sizeof(rolled_buff)));
	}
}