#include <text_roll.h>
#include <text_roll_hidden.h>
#include <disp1color.h>
#include <font.h>
#include <delay.h>
#include <stddef.h>
#include <string.h>

#define DELAY	2000

t_list	*head = NULL;

static void	update_buffers(t_list *node)
{
	t_rolled_buff	*buff = (t_rolled_buff*)node->content;
	uint8_t			tmp;
	
	if (buff->roll_direction == direction_left)
	{
		tmp = buff->buff[0];
		for (uint8_t i = 0; i != buff->buff_len; i++)
			buff->buff[i] = buff->buff[i + 1];
		buff->buff[buff->buff_len - 1] = tmp;
	}
	else
	{
		tmp = buff->buff[buff->buff_len - 1];
		for (uint8_t i = buff->buff_len - 1; i != 0; i--)
			buff->buff[i] = buff->buff[i - 1];
		buff->buff[0] = tmp;
	}
	memcpy(&disp1color_buff[buff->startX], buff->buff, buff->show_len);
}

void		roll_text(void)
{	
	disp1color_FillScreenbuff(0);
	ft_lstiter(head, update_buffers);
	disp1color_UpdateFromBuff();
	delay_ms(8);
}