/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdeloneif.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cervac <cervac@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/08 15:44:44 by cervac            #+#    #+#             */
/*   Updated: 2017/04/08 17:00:54 by cervac           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_list.h>

static void	del_node(t_list **parent_ptr,
	t_list **child_ptr, t_list **head, void (*del)(void *, size_t))
{
	if (NULL == *parent_ptr)
		*head = (*child_ptr)->next;
	else
		*parent_ptr = (*child_ptr)->next;
	ft_lstdelone(child_ptr, del);
}

int			ft_lstdeloneif(t_list **head, void (*del)(void *, size_t),
	int (*if_func)(t_list*, void *ptr), void *ptr)
{
	t_list	*parent_ptr;
	t_list	*child_ptr;
	t_list	*tmp_ptr;

	parent_ptr = NULL;
	child_ptr = *head;
	while (NULL != child_ptr)
	{
		tmp_ptr = child_ptr->next;
		if (if_func(child_ptr, ptr))
		{
			del_node(&parent_ptr, &child_ptr, head, del);
			return (1);
		}
		else
			parent_ptr = child_ptr;
		child_ptr = tmp_ptr;
	}
	return (0);
}
