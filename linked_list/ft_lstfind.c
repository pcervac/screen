/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cervac <cervac@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 20:51:51 by cervac            #+#    #+#             */
/*   Updated: 2017/04/06 20:57:41 by cervac           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_list.h>

t_list	*ft_lstfind(t_list *lst, int (*f_ptr)(t_list *, void *), void *prm)
{
	if (NULL == lst || NULL == f_ptr)
		return (NULL);
	while (NULL != lst)
	{
		if (f_ptr(lst, prm))
			return (lst);
		lst = lst->next;
	}
	return (NULL);
}
