/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cervac <cervac@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 19:04:09 by pcervac           #+#    #+#             */
/*   Updated: 2017/04/08 16:18:44 by cervac           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST_H
# define FT_LIST_H

# include <stdlib.h>
# include <string.h>
# include <stdio.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstaddb(t_list **alst, t_list *new);
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdelone(t_list **ast, void (*del)(void *, size_t));
int					ft_lstdelif(t_list **ast, void (*del)(void *, size_t),
						int (*if_func)(t_list*, void *ptr), void *ptr);
int					ft_lstdeloneif(t_list **ast, void (*del)(void *, size_t),
						int (*if_func)(t_list*, void *ptr), void *ptr);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list				*ft_lstnew(void const *content, size_t content_size);
t_list				*ft_lstfind(t_list *lst,
						int (*f_ptr)(t_list *, void *prm), void *prm);
#endif
