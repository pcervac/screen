#ifndef _DISP2COLOR_H
#define _DISP2COLOR_H

#include <types.h>
#include <disp1color.h>

void displcolor_printf_lines(uint8_t X, uint8_t Y, uint8_t FontID, const char *args, ...);
void disp1color_DrawString_lines(uint8_t X, uint8_t Y, uint8_t FontID, uint8_t *Str);

#endif //_DISP2COLOR_H