#include <stdarg.h>
#include <font.h>
#include <disp2color.h>
#include <stdio.h>

void displcolor_printf_lines(uint8_t X, uint8_t Y, uint8_t FontID, const char *args, ...)
{
  char StrBuff[100];
  
  va_list ap;
  va_start(ap, args);
  char len = vsnprintf(StrBuff, sizeof(StrBuff), args, ap);
  va_end(ap);
  
  disp1color_DrawString_lines(X, Y, FontID, (uint8_t *)StrBuff);
}

void disp1color_DrawString_lines(uint8_t X, uint8_t Y, uint8_t FontID, uint8_t *Str)
{
  uint8_t done = 0;             // ���� ��������� ������
  uint8_t Xstart = X;           // ���������� ���� ����� ���������� ������� ��� �������� �� ����� ������
  uint8_t StrHeight = 8;        // ������ �������� � �������� ��� �������� �� ��������� ������

  // ����� ������
  while (!done)
  {
    switch (*Str)
    {
    case '\0':  // ����� ������
      done = 1;
      break;
    case '\n':  // ������� �� ��������� ������
      Y += StrHeight;
      break;
    case '\r':  // ������� � ������ ������
      X = Xstart;
      break;
    default:    // ������������ ������
	  if (X + font_GetCharWidth(font_GetFontStruct(FontID, *Str)) > 128)
	  {
		Y += StrHeight;
		X = Xstart;
	  }
	  X += disp1color_DrawChar(X, Y, FontID, *Str);
      StrHeight = font_GetCharHeight(font_GetFontStruct(FontID, *Str));
      break;
    }
    Str++;
  }
}
